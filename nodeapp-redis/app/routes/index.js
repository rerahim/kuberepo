var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
#var db = mongojs('mongodb://mongo-0.mongo');
var db = mongojs('mongodb://mongodb:27017');

const monitor = require('../monitor.js');

router.get('/', function(req, res, next){
    console.log("root")
    monitor.connections.increment();     
    res.render('index.ejs');
});

router.get('/adduser', function(req, res, next){
    console.log("adduser")
    monitor.connections.increment();
    res.render('addUser.ejs');
});

router.post('/adduser', function(req, res, next){
    monitor.connections.increment();
    console.log("adduser")
    user=req.body
    db.users.save(user, function(err, user) {
       if(err) { 
            console.log(err); 
       }
    });
    res.render('addUser.ejs');
});

router.get('/listuser', function(req, res, next){
    console.log("listuser")
    monitor.connections.increment(); 
    db.users.find(function(err, users) {
       if(err) { 
            console.log(err); 
       }else {
            res.render('listUser.ejs', { "users": users });
       }
    });
});

module.exports = router;
 
