#!/bin/bash

if [ ! -e  /tmp/auth ]
then
  echo "run htpasswd -c /tmp/auth admin"
  exit
fi
#dd if=/dev/urandom bs=128 count=1 2>/dev/null | base64 | tr -d "=+/" | dd bs=32 count=1 2>/dev/null

####
export mgmt_user=vagrant
export interface=enp0s8
export apiserver_count=1
export prometheus_retention=15d
export domain=k8t.io
####

source ./version.sh.1.11.1

if [ -e  /etc/centos-release ]
then
  export docker_version=${centos_docker_version}
  export kubelet_version=${centos_kubelet_version}
  export kubectl_version=${centos_kubectl_version}
fi


ansible-playbook -i local-kube-inventory.ini step-3-kube-adm-user.yml


mkdir -p /var/tmp/addon/
rm /var/tmp/addon/*

ansible-playbook -i local-kube-inventory.ini  step-4-kube-addon.yml \
   -e kube_docker_version=${kube_docker_version} \
   -e kubedns_version=${kubedns_version} \
   -e weave_kube_version=${weave_kube_version} \
   -e kubernetes_dashboard_version=${kubernetes_dashboard_version} \
   -e prometheus_version=${prometheus_version} \
   -e calico_node=${calico_node} \
   -e calico_cni=${calico_cni} \
   -e calico_kube_policy_controller=${calico_kube_policy_controller} \
   -e domain=${domain} \
   -e helm_version=${helm_version} 

ansible-playbook -i local-kube-inventory.ini  step-5-kube-addon.yml \
   -e nginx_ingress_controller_version=${nginx_ingress_controller_version}  \
   -e prometheus_version=${prometheus_version} \
   -e prometheus_retention=${prometheus_retention} \
   -e domain=${domain} \
   -e grafana_version=${grafana_version} 

#ansible-playbook haproxy.yml -i local-kube-inventory.ini

sleep 15
##
kubectl create  secret generic basic-auth -n kube-system --from-file=/tmp/auth
kubectl create  secret generic basic-auth -n monitoring --from-file=/tmp/auth
kubectl create  secret generic basic-auth  --from-file=/tmp/auth

##
if [ ! -e  /tmp/tls.key ]
then
   openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /tmp/tls.key -out /tmp/tls.crt -subj "/CN=*.${domain}"
   kubectl create secret tls tls-secret -n kube-system --key /tmp/tls.key --cert /tmp/tls.crt
   kubectl create secret tls tls-secret -n monitoring --key /tmp/tls.key --cert /tmp/tls.crt
   kubectl create secret tls tls-secret  --key /tmp/tls.key --cert /tmp/tls.crt
fi

##
kubectl apply -f /var/tmp/addon/kubernetes-dashboard-ingress.yaml
kubectl apply -f /var/tmp/addon/prometheus-ingress.yaml
kubectl apply -f /var/tmp/addon/grafana-ingress.yaml

##
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

##
#kubectl taint nodes node1 key=value:NoSchedule
kubectl label node node1  node-role.kubernetes.io/master=

# Hao to remove a taint from node
#kubectl patch node node1.compute.internal -p '{"spec":{"taints":[]}}'

helm init
#kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
