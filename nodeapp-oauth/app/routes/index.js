const express = require('express');
const passport = require('passport');
const router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://mongo-0.mongo');

var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn();

const monitor = require('../monitor.js');

const env = {
  AUTH0_CLIENT_ID: process.env.AUTH0_CLIENT_ID,
  AUTH0_DOMAIN: process.env.AUTH0_DOMAIN,
  AUTH0_CALLBACK_URL:
    process.env.AUTH0_CALLBACK_URL || 'http://localhost:3000/callback'
};

/* GET home page. */

router.get('/login', passport.authenticate('auth0', {
  clientID: env.AUTH0_CLIENT_ID,
  domain: env.AUTH0_DOMAIN,
  redirectUri: env.AUTH0_CALLBACK_URL,
  responseType: 'code',
  audience: 'https://' + env.AUTH0_DOMAIN + '/userinfo',
  scope: 'openid profile'}),
  function(req, res) {
    res.redirect("/");
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/callback',
  passport.authenticate('auth0', {
    failureRedirect: '/failure'
  }),
  function(req, res) {
    res.render('index.ejs');
  }
);

router.get('/failure', function(req, res) {
  var error = req.flash("error");
  var error_description = req.flash("error_description");
  req.logout();
  res.render('failure', {
    error: error[0],
    error_description: error_description[0],
  });
});


router.get('/', ensureLoggedIn, function(req, res, next){
    console.log("root")
    monitor.connections.increment();
    res.render('index.ejs');
});

router.get('/adduser', ensureLoggedIn, function(req, res, next){
    console.log("adduser")
    monitor.connections.increment();
    res.render('addUser.ejs');
});


router.post('/adduser', function(req, res, next){
    monitor.connections.increment();
    console.log("adduser")
    user=req.body
    db.users.save(user, function(err, user) {
       if(err) {
            console.log(err);
       }
    });
    res.render('addUser.ejs');
});

router.get('/listuser', ensureLoggedIn, function(req, res, next){
    console.log("listuser")
    monitor.connections.increment();
    db.users.find(function(err, users) {
       if(err) {
            console.log(err);
       }else {
            res.render('listUser.ejs', { "users": users });
       }
    });
});

module.exports = router;
