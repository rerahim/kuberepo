#!/bin/bash

####
export mgmt_user=vagrant
export interface=enp0s8
export apiserver_count=1
####

source ./version.sh.1.9.1


ansible-playbook -i local-kube-inventory.ini  step-4-kube-addon.yml \
   -e kube_docker_version=${kube_docker_version} \
   -e kubedns_version=${kubedns_version} \
   -e weave_kube_version=${weave_kube_version} \
   -e kubernetes_dashboard_version=${kubernetes_dashboard_version} \
   -e prometheus_version=${prometheus_version} \
   -e calico_node=${calico_node} \
   -e calico_cni=${calico_cni} \
   -e helm_version=${helm_version} \
   -e calico_kube_policy_controller=${calico_kube_policy_controller} 

#ansible-playbook haproxy.yml -i local-kube-inventory.ini

sleep 15

kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

helm init
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'


