ssh dckreg sudo docker pull  gcr.io/google_containers/etcd-amd64:${etcd_docker_version} 
ssh dckreg sudo docker tag   gcr.io/google_containers/etcd-amd64:${etcd_docker_version} dckreg:5000/etcd-amd64:${etcd_docker_version}
ssh dckreg sudo docker push  dckreg:5000/etcd-amd64:${etcd_docker_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/kube-apiserver-amd64:${kube_docker_version} 
ssh dckreg sudo docker tag   gcr.io/google_containers/kube-apiserver-amd64:${kube_docker_version} dckreg:5000/kube-apiserver-amd64:${kube_docker_version}
ssh dckreg sudo docker push  dckreg:5000/kube-apiserver-amd64:${kube_docker_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/kube-controller-manager-amd64:${kube_docker_version} 
ssh dckreg sudo docker tag   gcr.io/google_containers/kube-controller-manager-amd64:${kube_docker_version} dckreg:5000/kube-controller-manager-amd64:${kube_docker_version}
ssh dckreg sudo docker push  dckreg:5000/kube-controller-manager-amd64:${kube_docker_version}

ssh dckreg sudo docker pull   gcr.io/google_containers/kube-scheduler-amd64:${kube_docker_version}
ssh dckreg sudo docker tag   gcr.io/google_containers/kube-scheduler-amd64:${kube_docker_version} dckreg:5000/kube-scheduler-amd64:${kube_docker_version}
ssh dckreg sudo docker push  dckreg:5000/kube-scheduler-amd64:${kube_docker_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/kube-proxy-amd64:${kube_docker_version}
ssh dckreg sudo docker tag   gcr.io/google_containers/kube-proxy-amd64:${kube_docker_version} dckreg:5000/kube-proxy-amd64:${kube_docker_version}
ssh dckreg sudo docker push  dckreg:5000/kube-proxy-amd64:${kube_docker_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/k8s-dns-kube-dns-amd64:${kubedns_version}
ssh dckreg sudo docker tag   gcr.io/google_containers/k8s-dns-kube-dns-amd64:${kubedns_version} dckreg:5000/k8s-dns-kube-dns-amd64:${kubedns_version}
ssh dckreg sudo docker push  dckreg:5000/k8s-dns-kube-dns-amd64:${kubedns_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/k8s-dns-dnsmasq-nanny-amd64:${kubedns_version}
ssh dckreg sudo docker tag   gcr.io/google_containers/k8s-dns-dnsmasq-nanny-amd64:${kubedns_version} dckreg:5000/k8s-dns-dnsmasq-nanny-amd64:${kubedns_version}
ssh dckreg sudo docker push  dckreg:5000/k8s-dns-dnsmasq-nanny-amd64:${kubedns_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/k8s-dns-sidecar-amd64:${kubedns_version}
ssh dckreg sudo docker tag   gcr.io/google_containers/k8s-dns-sidecar-amd64:${kubedns_version} dckreg:5000/k8s-dns-sidecar-amd64:${kubedns_version}
ssh dckreg sudo docker push  dckreg:5000/k8s-dns-sidecar-amd64:${kubedns_version}

ssh dckreg sudo docker pull  weaveworks/weave-kube:${weave_kube_version}
ssh dckreg sudo docker tag   weaveworks/weave-kube:${weave_kube_version} dckreg:5000/weave-kube:${weave_kube_version}
ssh dckreg sudo docker push  dckreg:5000/weave-kube:${weave_kube_version}

ssh dckreg sudo docker pull  weaveworks/weave-npc:${weave_kube_version}
ssh dckreg sudo docker tag   weaveworks/weave-npc:${weave_kube_version} dckreg:5000/weave-npc:${weave_kube_version}
ssh dckreg sudo docker push  dckreg:5000/weave-npc:${weave_kube_version}

ssh dckreg sudo docker pull  gcr.io/google_containers/kubernetes-dashboard-amd64:${kubernetes_dashboard_version}
ssh dckreg sudo docker tag   gcr.io/google_containers/kubernetes-dashboard-amd64:${kubernetes_dashboard_version} dckreg:5000/kubernetes-dashboard-amd64:${kubernetes_dashboard_version}
ssh dckreg sudo docker push  dckreg:5000/kubernetes-dashboard-amd64:${kubernetes_dashboard_version}

ssh dckreg sudo docker pull  quay.io/prometheus/prometheus:${prometheus_version}
ssh dckreg sudo docker tag   quay.io/prometheus/prometheus:${prometheus_version}  dckreg:5000/prometheus:${prometheus_version}
ssh dckreg sudo docker push  dckreg:5000/prometheus:${prometheus_version}

ssh dckreg sudo docker pull  docker.io/istio/istio-ca:${istio_version}
ssh dckreg sudo docker tag   docker.io/istio/istio-ca:${istio_version}  dckreg:5000/istio-ca:${istio_version}
ssh dckreg sudo docker push  dckreg:5000/istio-ca:${istio_version}

ssh dckreg sudo docker pull  docker.io/istio/pilot:${istio_version}
ssh dckreg sudo docker tag   docker.io/istio/pilot:${istio_version}  dckreg:5000/istio/pilot:${istio_version}
ssh dckreg sudo docker push  dckreg:5000/istio/pilot:${istio_version}

ssh dckreg sudo docker pull  docker.io/istio/mixer:${istio_version}
ssh dckreg sudo docker tag   docker.io/istio/mixer:${istio_version}  dckreg:5000/istio/mixer:${istio_version}
ssh dckreg sudo docker push  dckreg:5000/istio/mixer:${istio_version}

ssh dckreg sudo docker pull  docker.io/istio/proxy:${istio_version}
ssh dckreg sudo docker tag   docker.io/istio/proxy:${istio_version}  dckreg:5000/istio/proxy:${istio_version}
ssh dckreg sudo docker push  dckreg:5000/istio/proxy:${istio_version}

ssh dckreg sudo docker pull  prom/statsd-exporter:${statsd-exporter_version}
ssh dckreg sudo docker tag   prom/statsd-exporter:${statsd-exporter_version} dckreg:5000/prom/statsd-exporter:${statsd-exporter_version}
ssh dckreg sudo docker push  dckreg:5000/prom/statsd-exporter:${statsd-exporter_version}
